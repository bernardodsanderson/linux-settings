### Chromebook
Termux access download folder: `termux-setup-storage`


### Plex
`sudo chmod -R 777 /media/username`

https://support.plex.tv/hc/en-us/articles/201539237-Backing-Up-Plex-Media-Server-Data

https://support.plex.tv/hc/en-us/articles/201370363-Move-an-Install-to-Another-System

https://support.plex.tv/hc/en-us/articles/202915258

`/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/`

### Elementary Apps
- Gimp
- Firefox
- Chromium
- Makemkv
- VSCode
- Monitor
- Handbrake
- Gnome Disks
- Eddy
- Steam
- Torrential
- VLC
- Screencast
- Sublime Text
- Plex
- Color Picker
- Mullvad

### Apt-get
- git, ruby, checkinstall, vim, fish, filezilla

## Command Line

sudo add-apt-repository ppa:stebbins/handbrake-releases

sudo apt-get update

sudo apt-get upgrade

sudo apt-get install handbrake-gtk

sudo apt-get install -y software-properties-common

### Node
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

sudo apt-get install -y nodejs


### MakeMKV
http://lifeonubuntu.com/ubuntu-missing-add-apt-repository-command/

http://ubuntuhandbook.org/index.php/2016/03/install-makemkv-play-dvd-blurays/


[Link - MakeMKV Forum](http://www.makemkv.com/forum2/viewtopic.php?f=3&t=224)


## Frequently Used Commands

**React Native**

react-native run-android


react-native start --reset-cache


**apt-get**


sudo apt-get update && sudo apt-get upgrade


**plex**

service plexmediaserver start


service plexmediaserver start


**ffmpeg**


Convert a movie


`ffmpeg -i input.mkv -preset veryslow -crf 24 -c:s copy -c:v libx264 -c:a libvo_aacenc -b:a 128k output.mkv`


Take snapshot of movie

`ffmpeg -y -ss 3000 -i "clipname.mov" -vframes 1 "clipScreenshot001.png"`

